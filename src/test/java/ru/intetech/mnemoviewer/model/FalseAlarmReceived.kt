package ru.intetech.mnemoviewer.model

import org.jetbrains.spek.api.Spek
import org.joda.time.DateTime
import ru.intetech.mnemoviewer.ui.model.alarms.AlarmsModel
import kotlin.test.assertFalse

class FalseAlarmReceived: Spek ({
    describe("Received false alarm") {
        val tagName = "tag1"
        val alarmsModel = AlarmsModel("testUser")

        it("result of adding false boolean alarm") {
            val date = DateTime.now()
            val newTableRow = AlarmsModel.TableRow(date.toDate(), tagName, null)
            alarmsModel.onAlarmReceived(TagModel(tagName, false, date.toString()))
            assertFalse {
                alarmsModel.alertModel.unconfirmedAlarms.contains(newTableRow) ||
                        alarmsModel.tableModel.alarms.contains(newTableRow)
            }
        }

        it("result of adding false int alarm") {
            val date = DateTime.now()
            val newTableRow = AlarmsModel.TableRow(date.toDate(), tagName, null)
            alarmsModel.onAlarmReceived(TagModel(tagName, 0, date.toString()))
            assertFalse {
                alarmsModel.alertModel.unconfirmedAlarms.contains(newTableRow) ||
                        alarmsModel.tableModel.alarms.contains(newTableRow)
            }
        }

        it("result of adding false double alarm") {
            val date = DateTime.now()
            val newTableRow = AlarmsModel.TableRow(date.toDate(), tagName, null)
            alarmsModel.onAlarmReceived(TagModel(tagName, 0.0, date.toString()))
            assertFalse {
                alarmsModel.alertModel.unconfirmedAlarms.contains(newTableRow) ||
                        alarmsModel.tableModel.alarms.contains(newTableRow)
            }
        }
    }
})