package ru.intetech.mnemoviewer.model

import org.jetbrains.spek.api.Spek
import org.joda.time.DateTime
import ru.intetech.mnemoviewer.ui.model.alarms.AlarmsModel
import kotlin.test.assertTrue

class TrueAlarmReceived: Spek({
    describe("Received true alarm") {
        val tagName = "tag1"
        val alarmsModel = AlarmsModel("testUser")

        it("result of adding true boolean alarm") {
            val date = DateTime.now()
            val newTableRow = AlarmsModel.TableRow(date.toDate(), tagName, null)
            alarmsModel.onAlarmReceived(TagModel(tagName, true, date.toString()))
            assertTrue {
                alarmsModel.alertModel.unconfirmedAlarms.contains(newTableRow) &&
                        alarmsModel.tableModel.alarms.contains(newTableRow)
            }
        }

        it("result of adding true int alarm") {
            val date = DateTime.now()
            val newTableRow = AlarmsModel.TableRow(date.toDate(), tagName, null)
            alarmsModel.onAlarmReceived(TagModel(tagName, 1, date.toString()))
            assertTrue {
                alarmsModel.alertModel.unconfirmedAlarms.contains(newTableRow) &&
                        alarmsModel.tableModel.alarms.contains(newTableRow)
            }
        }

        it("result of adding true double alarm") {
            val date = DateTime.now()
            val newTableRow = AlarmsModel.TableRow(date.toDate(), tagName, null)
            alarmsModel.onAlarmReceived(TagModel(tagName, 1.0, date.toString()))
            assertTrue {
                alarmsModel.alertModel.unconfirmedAlarms.contains(newTableRow) &&
                        alarmsModel.tableModel.alarms.contains(newTableRow)
            }
        }
    }
})