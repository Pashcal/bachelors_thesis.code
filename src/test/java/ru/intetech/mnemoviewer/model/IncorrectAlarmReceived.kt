package ru.intetech.mnemoviewer.model

import org.jetbrains.spek.api.Spek
import org.joda.time.DateTime
import ru.intetech.mnemoviewer.ui.model.alarms.AlarmsModel
import kotlin.test.assertFalse

class IncorrectAlarmReceived: Spek ({
    describe("Received incorrect alarm") {
        val tagName = "tag1"
        val alarmsModel = AlarmsModel("testUser")

        it("result of adding incorrect int alarm") {
            val date = DateTime.now()
            val newTableRow = AlarmsModel.TableRow(date.toDate(), tagName, null)
            alarmsModel.onAlarmReceived(TagModel(tagName, 42, date.toString()))
            assertFalse {
                alarmsModel.alertModel.unconfirmedAlarms.contains(newTableRow) ||
                        alarmsModel.tableModel.alarms.contains(newTableRow)
            }
        }

        it("result of adding incorrect double alarm") {
            val date = DateTime.now()
            val newTableRow = AlarmsModel.TableRow(date.toDate(), tagName, null)
            alarmsModel.onAlarmReceived(TagModel(tagName, 9.9, date.toString()))
            assertFalse {
                alarmsModel.alertModel.unconfirmedAlarms.contains(newTableRow) ||
                        alarmsModel.tableModel.alarms.contains(newTableRow)
            }
        }

        it("result of adding string alarm") {
            val date = DateTime.now()
            val newTableRow = AlarmsModel.TableRow(date.toDate(), tagName, null)
            alarmsModel.onAlarmReceived(TagModel(tagName, "42", date.toString()))
            assertFalse {
                alarmsModel.alertModel.unconfirmedAlarms.contains(newTableRow) ||
                        alarmsModel.tableModel.alarms.contains(newTableRow)
            }
        }
    }
})