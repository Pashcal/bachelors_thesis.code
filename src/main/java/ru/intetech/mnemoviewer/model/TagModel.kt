package ru.intetech.mnemoviewer.model

data class TagModel(val name: String, val value: Any, val timestamp: String)