package ru.intetech.mnemoviewer.ui

import com.vaadin.annotations.VaadinServletConfiguration
import com.vaadin.server.*
import ru.intetech.mnemoviewer.driver.InfluxDriver
import ru.intetech.mnemoviewer.driver.RedisDriver
import javax.servlet.annotation.WebServlet

@WebServlet(urlPatterns = arrayOf("/*"), name = "MainUIServlet", asyncSupported = true)
@VaadinServletConfiguration(ui = MnemoViewerUI::class, productionMode = true, heartbeatInterval = 60)
class MnemoViewerServlet: VaadinServlet(), SessionInitListener, SessionDestroyListener {

    override fun servletInitialized() {
        service.addSessionInitListener(this);
        service.addSessionDestroyListener(this);
        super.servletInitialized()
    }

    override fun sessionInit(event: SessionInitEvent?) {
        if (event == null)
            return
        event.session.setAttribute("user", "TestUsername")
    }

    override fun sessionDestroy(event: SessionDestroyEvent?) {
        if (event == null)
            return
        event.session.setAttribute("user", null)
    }
}