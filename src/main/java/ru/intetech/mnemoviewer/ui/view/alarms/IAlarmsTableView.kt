package ru.intetech.mnemoviewer.ui.view.alarms

import com.vaadin.ui.Component
import ru.intetech.mnemoviewer.ui.model.alarms.AlarmsModel

interface IAlarmsTableView: Component {
    fun setDataSource(data: Collection<AlarmsModel.TableRow>)
    fun addAlarm(alarm: AlarmsModel.TableRow)
}