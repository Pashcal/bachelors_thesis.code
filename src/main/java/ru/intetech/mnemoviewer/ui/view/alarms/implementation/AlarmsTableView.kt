package ru.intetech.mnemoviewer.ui.view.alarms.implementation

import com.vaadin.data.Container
import com.vaadin.data.Item
import com.vaadin.data.Property
import com.vaadin.data.util.IndexedContainer
import com.vaadin.shared.ui.datefield.Resolution
import com.vaadin.ui.*
import com.vaadin.ui.themes.ValoTheme
import org.joda.time.DateTime
import org.joda.time.Interval
import ru.intetech.mnemoviewer.ui.MnemoViewerUI
import ru.intetech.mnemoviewer.ui.model.alarms.AlarmsModel
import ru.intetech.mnemoviewer.ui.view.alarms.IAlarmsTableView
import java.text.SimpleDateFormat
import java.util.*

class AlarmsTableView: GridLayout(1, 2), IAlarmsTableView {

    private val labels = MnemoViewerUI.labels

    private val filterSDField: PopupDateField
    private val filterEDField: PopupDateField
    private val filterSField: TextField

    private val table: Table
    private val tableContainer: Container.Filterable

    private var dateFilter: Container.Filter? = null
    private var textFilter: Container.Filter? = null

    init {
        setSizeFull()
        setRowExpandRatio(1, 1f)

        val filterLayout = GridLayout(5, 1)
        filterLayout.defaultComponentAlignment = Alignment.MIDDLE_CENTER
        filterLayout.setColumnExpandRatio(0, .17f)
        filterLayout.setColumnExpandRatio(1, .01f)
        filterLayout.setColumnExpandRatio(2, .17f)
        filterLayout.setColumnExpandRatio(3, .42f)
        filterLayout.setColumnExpandRatio(4, .35f)
        filterLayout.setWidth("100%")
        filterLayout.setMargin(true)
        filterLayout.isSpacing = true

        filterSDField = PopupDateField()
        filterSDField.locale = UI.getCurrent().locale
        filterSDField.resolution = Resolution.SECOND
        filterSDField.dateFormat = "dd.MM.yyyy HH:mm:ss"
        filterSDField.inputPrompt = labels.getProperty("alarms.TableView.fStartDatePrompt")
        filterSDField.setWidth("100%")

        filterEDField = PopupDateField()
        filterEDField.locale = UI.getCurrent().locale
        filterEDField.resolution = Resolution.SECOND
        filterEDField.dateFormat = "dd.MM.yyyy HH:mm:ss"
        filterEDField.inputPrompt = labels.getProperty("alarms.TableView.fEndDatePrompt")
        filterEDField.setWidth("100%")

        filterSField = TextField()
        filterSField.inputPrompt = labels.getProperty("alarms.TableView.fStringPrompt")
        filterSField.setWidth("100%")

        filterLayout.addComponent(filterSDField, 0, 0)
        filterLayout.addComponent(Label("—"), 1, 0)
        filterLayout.addComponent(filterEDField, 2, 0)
        filterLayout.addComponent(filterSField, 4, 0)

        tableContainer = IndexedContainer()
        tableContainer.addContainerProperty("time", Date::class.java, null)
        tableContainer.addContainerProperty("name", String::class.java, null)
        tableContainer.addContainerProperty("description", String::class.java, null)

        table = FormattedTable()
        table.setSizeFull()
        table.addStyleName(ValoTheme.TABLE_BORDERLESS)
        table.addStyleName(ValoTheme.TABLE_NO_HORIZONTAL_LINES)
        table.addStyleName(ValoTheme.TABLE_COMPACT)
        table.isImmediate = true
        table.isSortEnabled = true

        table.containerDataSource = tableContainer

        table.setVisibleColumns("time", "name", "description")
        table.setColumnHeaders(labels.getProperty("alarms.TableView.header.description"),
                labels.getProperty("alarms.TableView.header.name"),
                labels.getProperty("alarms.TableView.header.description"))
        table.setColumnExpandRatio("description", 1f)

        addComponent(filterLayout, 0, 0)
        addComponent(table, 0, 1)

        val dateFiledListener = Property.ValueChangeListener {
            if (filterSDField.value != null && filterEDField.value != null &&
                    DateTime(filterSDField.value).isBefore(DateTime(filterEDField.value)))
                setDateFilter(Interval(DateTime(filterSDField.value), DateTime(filterEDField.value)))
            else if (dateFilter != null) {
                tableContainer.removeContainerFilter(dateFilter)
                dateFilter = null
            }
        }
        filterSDField.addValueChangeListener(dateFiledListener)
        filterEDField.addValueChangeListener(dateFiledListener)
        filterSField.addTextChangeListener {
            if (!it.text.isNullOrEmpty())
                setTextFilter(it.text)
            else if (textFilter != null) {
                tableContainer.removeContainerFilter(textFilter)
                textFilter = null
            }
        }
    }

    class FormattedTable : Table() {
        val dateFormat = SimpleDateFormat("dd.MM.yyyy HH:mm:ss")
        override fun formatPropertyValue(rowId: Any?, colId: Any, property: Property<*>?): String? {
            var result = super.formatPropertyValue(rowId, colId, property)
            if (colId == "time" && property?.value != null)
                result = dateFormat.format(property!!.value as Date)
            return result
        }
    }

    override fun setDataSource(data: Collection<AlarmsModel.TableRow>) {
        data.forEach {
            val newItem = tableContainer.getItem(tableContainer.addItem())
            newItem.getItemProperty("time").value = it.time
            newItem.getItemProperty("name").value = it.name
            newItem.getItemProperty("description").value = it.description
        }
    }

    override fun addAlarm(alarm: AlarmsModel.TableRow) {
        val newItemId = tableContainer.addItem()
        val newItem = tableContainer.getItem(newItemId)
        if (newItem != null) {
            newItem.getItemProperty("time").value = alarm.time
            newItem.getItemProperty("name").value = alarm.name
            newItem.getItemProperty("description").value = alarm.description
            table.sort(arrayOf("time"), booleanArrayOf(false))
        }
    }

    private fun setDateFilter(filterDateInterval: Interval) {
        tableContainer.removeContainerFilter(dateFilter)
        dateFilter = object: Container.Filter {
            override fun passesFilter(itemId: Any,
                                      item: Item): Boolean {

                return filterByDateInterval("time", item, filterDateInterval)
            }

            override fun appliesToProperty(propertyId: Any): Boolean {
                if ("time".equals(propertyId))
                    return true

                return false
            }
        }
        tableContainer.addContainerFilter(dateFilter)
    }

    private fun filterByDateInterval(prop: String, item: Item?, dateInterval: Interval): Boolean {
        if (item?.getItemProperty(prop)?.value == null)
            return false

        val v = item!!.getItemProperty(prop).value as? Date
        if (v != null && dateInterval.contains(DateTime(v)))
            return true

        return false
    }

    private fun setTextFilter(filterText: String) {
        tableContainer.removeContainerFilter(textFilter)
        textFilter = object: Container.Filter {
                override fun passesFilter(itemId: Any,
                                          item: Item): Boolean {

                    return filterByProperty("name", item, filterText)
                            || filterByProperty("description", item, filterText)
                }

                override fun appliesToProperty(propertyId: Any): Boolean {
                    if (arrayOf("name", "description").any { it.equals(propertyId) })
                        return true

                    return false
                }
            }
        tableContainer.addContainerFilter(textFilter)
    }

    private fun filterByProperty(prop: String, item: Item?, text: String): Boolean {
        if (item?.getItemProperty(prop)?.value == null)
            return false

        val v = item!!.getItemProperty(prop).value.toString().trim(' ').toLowerCase()
        val filterText = text.toLowerCase()
        if (filterText.split("[ ,;]".toRegex()).any { !it.isEmpty() && v.contains(it) })
            return true

        return false
    }
}