package ru.intetech.mnemoviewer.ui.view.alarms

import com.vaadin.ui.Component
import ru.intetech.mnemoviewer.ui.model.alarms.AlarmsModel

interface IAlarmsAlertView: Component {
    fun addAlarm(alarm: AlarmsModel.TableRow)

    interface ConfirmAlarmListener {
        fun onAlarmConfirmed(alarmName: String)
    }
    val confirmListeners: MutableList<ConfirmAlarmListener>
    fun addConfirmListener(confirmListener: ConfirmAlarmListener)

    interface ConfirmAllAlarmsListener {
        fun onAllAlarmsConfirmed()
    }
    val confirmAllListeners: MutableList<ConfirmAllAlarmsListener>
    fun addConfirmAllListener(confirmAllListener: ConfirmAllAlarmsListener)
}