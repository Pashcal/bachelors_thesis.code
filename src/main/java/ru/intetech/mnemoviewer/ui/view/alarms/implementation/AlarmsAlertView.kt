package ru.intetech.mnemoviewer.ui.view.alarms.implementation

import com.vaadin.data.Container
import com.vaadin.data.util.IndexedContainer
import com.vaadin.event.ShortcutAction
import com.vaadin.server.FontAwesome
import com.vaadin.server.Sizeable
import com.vaadin.ui.*
import com.vaadin.ui.themes.ValoTheme
import ru.intetech.mnemoviewer.ui.MnemoViewerUI
import ru.intetech.mnemoviewer.ui.model.alarms.AlarmsModel
import ru.intetech.mnemoviewer.ui.view.alarms.IAlarmsAlertView
import java.text.SimpleDateFormat
import java.util.*

class AlarmsAlertView: Button(), IAlarmsAlertView, AutoCloseable {

    private val dateFormat = SimpleDateFormat("dd.MM.yyyy HH:mm:ss")
    private val labels = MnemoViewerUI.labels
    override val confirmListeners: MutableList<IAlarmsAlertView.ConfirmAlarmListener> = mutableListOf()
    override val confirmAllListeners: MutableList<IAlarmsAlertView.ConfirmAllAlarmsListener> = mutableListOf()

    private val window: Window
    private val table: Table
    private val tableContainer: Container.Indexed

    init {
        addStyleName(ValoTheme.BUTTON_ICON_ONLY)
        addStyleName(ValoTheme.BUTTON_BORDERLESS)
        addStyleName("alert-view-btn")
        icon = FontAwesome.BELL

        table = Table()
        tableContainer = IndexedContainer()
        tableContainer.addContainerProperty("view", Panel::class.java, null)
        tableContainer.addContainerProperty("time", Date::class.java, null)
        tableContainer.addContainerProperty("name", String::class.java, null)
        tableContainer.addContainerProperty("description", String::class.java, null)

        window = Window()
        buildWindow()

        addClickListener {
            if (window.isAttached)
                window.close()
            else if (!caption.isNullOrEmpty())
            {
                window.positionX = it.clientX - it.relativeX + 40
                window.positionY = it.clientY - it.relativeY - 200
                ui.addWindow(window)
                window.focus()

//                (ui.content as HorizontalLayout).addLayoutClickListener(object: LayoutEvents.LayoutClickListener {
//                    override fun layoutClick(p0: LayoutEvents.LayoutClickEvent?) {
//                        window.close()
//                        (ui.content as HorizontalLayout).removeLayoutClickListener(this)
//                    }
//                })
            }
        }
    }

    private fun buildWindow() {
        val windowsLayout = VerticalLayout()
        windowsLayout.setSizeFull()

        val title = Label(labels.getProperty("alarms.AlertView.windowTitle"))
        title.addStyleName(ValoTheme.LABEL_H3)
        title.addStyleName(ValoTheme.LABEL_NO_MARGIN)
        windowsLayout.addComponent(title)

        table.setSizeFull()
        table.addStyleName(ValoTheme.TABLE_BORDERLESS)
        table.addStyleName(ValoTheme.TABLE_NO_HEADER)
        table.addStyleName(ValoTheme.TABLE_NO_HORIZONTAL_LINES)
        table.addStyleName(ValoTheme.TABLE_NO_VERTICAL_LINES)
        table.addStyleName(ValoTheme.TABLE_NO_STRIPES)

        table.isImmediate = true
        table.containerDataSource = tableContainer
        table.setVisibleColumns("view")

        val footer = HorizontalLayout()
        footer.addStyleName(ValoTheme.WINDOW_BOTTOM_TOOLBAR)
        footer.setWidth("100%")
        val confirmAll = Button(labels.getProperty("alarms.AlertView.confirmAllBtn"), ClickListener {
            UI.getCurrent().access {
                tableContainer.removeAllItems()
                window.close()
            }
            confirmAllListeners.forEach {
                it.onAllAlarmsConfirmed()
            }
        })
        confirmAll.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED)
        confirmAll.addStyleName(ValoTheme.BUTTON_SMALL)
        footer.addComponent(confirmAll)
        footer.setComponentAlignment(confirmAll, Alignment.TOP_CENTER)

        windowsLayout.addComponents(title, table, footer)
        windowsLayout.setExpandRatio(table, 1f)

        window.setWidth(300.0f, Sizeable.Unit.PIXELS)
        window.setHeight(500.0f, Sizeable.Unit.PIXELS)
        window.addStyleName("alert-view-window")
        window.isClosable = true
        window.isResizable = false
        window.isDraggable = false
        window.setCloseShortcut(ShortcutAction.KeyCode.ESCAPE)
        window.content = windowsLayout
    }

    override fun addAlarm(alarm: AlarmsModel.TableRow) {
        val newItemId = tableContainer.addItem()
        val newItem = tableContainer.getItem(newItemId)
        if (newItem != null) {
            newItem.getItemProperty("view").value = buildAlarmView(alarm)
            newItem.getItemProperty("time").value = alarm.time
            newItem.getItemProperty("name").value = alarm.name
            newItem.getItemProperty("description").value = alarm.description
            table.sort(arrayOf("time"), booleanArrayOf(false))
            table.currentPageFirstItemId = newItemId
        }
    }

    private fun buildAlarmView(alarm: AlarmsModel.TableRow): Panel {
        val view = Panel(dateFormat.format(alarm.time))

        val viewContent = VerticalLayout()
        viewContent.setMargin(true)
        viewContent.isSpacing = true

        val name = Label(alarm.name)
        name.setSizeUndefined()
        name.addStyleName(ValoTheme.LABEL_BOLD)
        name.addStyleName(ValoTheme.LABEL_COLORED)

        val description = Label(alarm.description)
        description.setSizeFull()

        val confirmBtn = Button(labels.getProperty("alarms.AlertView.confirmBtn"), ClickListener {
            val confirmedItems: MutableList<Any?> = mutableListOf()
            tableContainer.itemIds.forEach {
                val item = tableContainer.getItem(it)
                if (alarm.name.equals(item.getItemProperty("name").value))
                    confirmedItems.add(it)
            }
            UI.getCurrent().access {
                confirmedItems.forEach {
                    tableContainer.removeItem(it)
                }
                if (tableContainer.size() == 0)
                    window.close()
            }
            confirmListeners.forEach {
                it.onAlarmConfirmed(alarm.name)
            }
        })
        confirmBtn.addStyleName(ValoTheme.BUTTON_PRIMARY)
        confirmBtn.addStyleName(ValoTheme.BUTTON_SMALL)

        viewContent.addComponents(name, description, confirmBtn)
        viewContent.setComponentAlignment(name, Alignment.TOP_RIGHT)
        viewContent.setComponentAlignment(confirmBtn, Alignment.BOTTOM_RIGHT)
        view.content = viewContent
        return view
    }

    override fun addConfirmListener(confirmListener: IAlarmsAlertView.ConfirmAlarmListener) {
        confirmListeners.add(confirmListener)
    }

    override fun addConfirmAllListener(confirmAllListener: IAlarmsAlertView.ConfirmAllAlarmsListener) {
        confirmAllListeners.add(confirmAllListener)
    }

    override fun close() {
        confirmListeners.clear()
    }
}