package ru.intetech.mnemoviewer.ui

import com.google.common.annotations.VisibleForTesting
import com.vaadin.annotations.*
import com.vaadin.server.*
import com.vaadin.shared.communication.PushMode
import com.vaadin.ui.*
import ru.intetech.mnemoviewer.ui.model.alarms.AlarmsModel
import ru.intetech.mnemoviewer.ui.presenter.alarms.AlarmsPresenter
import ru.intetech.mnemoviewer.ui.view.alarms.implementation.AlarmsAlertView
import ru.intetech.mnemoviewer.ui.view.alarms.implementation.AlarmsTableView
import ru.intetech.mnemoviewer.utils.fromJson
import java.io.FileReader
import java.util.*

@Theme("mnemoviewer")
@Title("Intetech MnemoViewer")
@Widgetset("Widgetset")
@Push(PushMode.AUTOMATIC)
@PreserveOnRefresh
class MnemoViewerUI: UI() {

    private object _alarmTags {
        @JvmStatic var instance: Map<String, String>? = null
        fun get(): Map<String, String> = instance ?: emptyMap()
        fun set(value: Map<String, String>) {
            if (instance == null)
                instance = value
        }
    }

    private object _labels {
        @JvmStatic var instance: Properties? = null
        fun get(): Properties = instance ?: Properties()
        fun set(value: Properties) {
            if (instance == null)
                instance = value
        }
    }

    var alarmsModel: AlarmsModel? = null
    var alarmsTableView: AlarmsTableView? = null
    var alarmsAlertView: AlarmsAlertView? = null

    companion object {
        var resourcePath: String = ""
            private set
            get
        var redisHost: String = ""
            private set
            get
        var influxHost: String = ""
            private set
            get
        val alarmTags: Map<String, String>
            get() = _alarmTags.get()
        val labels: Properties
            get() = _labels.get()
    }

    override fun init(vaadinRequest: VaadinRequest) {
        UI.getCurrent().locale = Locale("ru", "RU")
        resourcePath = "${VaadinService.getCurrent().baseDirectory.absolutePath}\\WEB-INF\\classes\\resources"

        val configProp = Properties()
        configProp.load(FileReader("$resourcePath\\config\\config.properties"))
        redisHost = configProp.getProperty("redis.host")
        influxHost = configProp.getProperty("influx.host")

        val labelsProp = Properties()
        labelsProp.load(FileReader("$resourcePath\\label\\labels_ru.properties"))
        _labels.set(labelsProp)

        if (alarmTags.isEmpty())
            loadAlarmTags()

        alarmsModel = AlarmsModel(UI.getCurrent().session.getAttribute("user").toString())
        alarmsTableView = AlarmsTableView()
        alarmsAlertView  = AlarmsAlertView()
        AlarmsPresenter(alarmsModel!!, alarmsTableView!!, alarmsAlertView!!)

        val testLayout = HorizontalLayout()
        testLayout.setSizeFull()
        testLayout.setMargin(true)
        testLayout.isSpacing = true
        testLayout.defaultComponentAlignment = Alignment.MIDDLE_LEFT
        testLayout.addComponents(alarmsAlertView, alarmsTableView)
        testLayout.setExpandRatio(alarmsTableView, 1f)

        content = testLayout
    }

    data class PropTagModel(val isAlarm: Boolean)
    private fun loadAlarmTags() {
        val aTags = mutableMapOf<String, String>()

        val tagsProp = Properties()
        tagsProp.load(FileReader("$resourcePath\\config\\tags.properties"))
        tagsProp.forEach {
            p -> if (p.value.toString().fromJson<PropTagModel>().isAlarm) {
                aTags[p.key.toString()] = labels.getProperty("tags.description.${p.key.toString()}")
            }
        }

        _alarmTags.set(aTags)
    }

    override fun close() {
        alarmsModel?.close()
        alarmsModel = null
        session.close()
        super.close()
    }
}