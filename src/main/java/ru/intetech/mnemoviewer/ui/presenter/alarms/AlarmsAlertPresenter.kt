package ru.intetech.mnemoviewer.ui.presenter.alarms

import com.vaadin.ui.UI
import org.joda.time.DateTime
import ru.intetech.mnemoviewer.model.TagModel
import ru.intetech.mnemoviewer.ui.MnemoViewerUI
import ru.intetech.mnemoviewer.ui.model.alarms.AlarmsAlertModel
import ru.intetech.mnemoviewer.ui.model.alarms.AlarmsListeningModel
import ru.intetech.mnemoviewer.ui.model.alarms.AlarmsModel
import ru.intetech.mnemoviewer.ui.model.alarms.AlarmsTableModel
import ru.intetech.mnemoviewer.ui.view.alarms.IAlarmsAlertView

class AlarmsAlertPresenter(private val model: AlarmsAlertModel,
                           private val view: IAlarmsAlertView) {

    init {
        model.addAlarmListener(object: AlarmsListeningModel.AlarmListener {
            override fun onAlarmReceived(tag: TagModel) {
                UI.getCurrent().access {
                    view.addAlarm(AlarmsModel.TableRow(DateTime.parse(tag.timestamp).toDate(),
                            tag.name, MnemoViewerUI.alarmTags[tag.name]))
                    view.caption = model.unconfirmedAlarms.count().toString()
                    if (!view.isVisible)
                        view.isVisible = true
                }
            }
        })
        view.addConfirmListener(object: IAlarmsAlertView.ConfirmAlarmListener {
            override fun onAlarmConfirmed(alarmName: String) {
                model.confirmAlarm(alarmName)
                if (model.unconfirmedAlarms.isEmpty()) {
                    view.isVisible = false
                    view.caption = null
                }
                else {
                    view.caption = model.unconfirmedAlarms.count().toString()
                }
            }
        })
        view.addConfirmAllListener(object: IAlarmsAlertView.ConfirmAllAlarmsListener {
            override fun onAllAlarmsConfirmed() {
                model.confirmAllAlarms()
                view.isVisible = false
                view.caption = null
            }
        })

        if (model.unconfirmedAlarms.isEmpty()) {
            view.isVisible = false
            view.caption = null
        }
        else {
            view.caption = model.unconfirmedAlarms.count().toString()
            model.unconfirmedAlarms.forEach {
                view.addAlarm(it)
            }
        }
    }
}