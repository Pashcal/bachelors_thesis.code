package ru.intetech.mnemoviewer.ui.presenter.alarms

import ru.intetech.mnemoviewer.ui.model.alarms.AlarmsModel
import ru.intetech.mnemoviewer.ui.view.alarms.IAlarmsAlertView
import ru.intetech.mnemoviewer.ui.view.alarms.IAlarmsTableView

class AlarmsPresenter(private val model: AlarmsModel,
                      private val tableView: IAlarmsTableView,
                      private val alertView: IAlarmsAlertView) {
    init {
        AlarmsTablePresenter(model.tableModel, tableView)
        AlarmsAlertPresenter(model.alertModel, alertView)
    }
}