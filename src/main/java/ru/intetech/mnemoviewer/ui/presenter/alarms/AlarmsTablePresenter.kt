package ru.intetech.mnemoviewer.ui.presenter.alarms

import com.vaadin.ui.UI
import org.joda.time.DateTime
import ru.intetech.mnemoviewer.model.TagModel
import ru.intetech.mnemoviewer.ui.MnemoViewerUI
import ru.intetech.mnemoviewer.ui.model.alarms.AlarmsListeningModel
import ru.intetech.mnemoviewer.ui.model.alarms.AlarmsModel
import ru.intetech.mnemoviewer.ui.model.alarms.AlarmsTableModel
import ru.intetech.mnemoviewer.ui.view.alarms.IAlarmsTableView

class AlarmsTablePresenter(private val model: AlarmsTableModel,
                           private val view: IAlarmsTableView) {

    init {
        synchronized(model.alarms) {
            view.setDataSource(model.alarms)
        }
        model.addAlarmListener(object: AlarmsListeningModel.AlarmListener {
            override fun onAlarmReceived(tag: TagModel) {
                UI.getCurrent().access {
                    view.addAlarm(AlarmsModel.TableRow(DateTime.parse(tag.timestamp).toDate(),
                            tag.name, MnemoViewerUI.alarmTags[tag.name]))
                }
            }
        })
    }
}