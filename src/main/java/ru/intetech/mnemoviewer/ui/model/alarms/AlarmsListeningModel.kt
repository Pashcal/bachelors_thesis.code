package ru.intetech.mnemoviewer.ui.model.alarms

import ru.intetech.mnemoviewer.model.TagModel

open class AlarmsListeningModel: AutoCloseable {
    interface AlarmListener {
        fun onAlarmReceived(tag: TagModel)
    }

    private val alarmListeners: MutableList<AlarmListener> = mutableListOf()

    fun addAlarmListener(alarmListener: AlarmListener) {
        alarmListeners.add(alarmListener)
    }

    open fun onAlarmReceived(tag: TagModel) {
        alarmListeners.forEach {
            it.onAlarmReceived(tag)
        }
    }

    override fun close() {
        alarmListeners.clear()
    }
}