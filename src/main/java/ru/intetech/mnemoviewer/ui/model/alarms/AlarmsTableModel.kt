package ru.intetech.mnemoviewer.ui.model.alarms

import org.joda.time.DateTime
import ru.intetech.mnemoviewer.driver.InfluxDriver
import ru.intetech.mnemoviewer.model.TagModel
import ru.intetech.mnemoviewer.ui.MnemoViewerUI
import kotlin.concurrent.thread

class AlarmsTableModel: AlarmsListeningModel() {

    private val influx = InfluxDriver(MnemoViewerUI.influxHost)
    val alarms: MutableList<AlarmsModel.TableRow> = mutableListOf()

    init {
        thread {
            synchronized(alarms) {
                val tags = influx.getPeriod("Alarms", DateTime.now().minusMinutes(10), DateTime.now())
                if (tags != null)
                    initData(tags)
            }
        }
    }

    fun initData(tags: Collection<TagModel>) {
        synchronized(alarms) {
            tags.forEach {
                t -> alarms.add(0, AlarmsModel.TableRow(DateTime.parse(t.timestamp).toDate(), t.name,
                    MnemoViewerUI.alarmTags[t.name]))
            }
        }
    }

    override fun onAlarmReceived(tag: TagModel) {
        synchronized(alarms) {
            alarms.add(0, AlarmsModel.TableRow(DateTime.parse(tag.timestamp).toDate(), tag.name,
                    MnemoViewerUI.alarmTags[tag.name]))
            super.onAlarmReceived(tag)
        }
    }
}