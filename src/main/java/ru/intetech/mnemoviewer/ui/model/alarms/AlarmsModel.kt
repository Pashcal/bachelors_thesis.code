package ru.intetech.mnemoviewer.ui.model.alarms

import ru.intetech.mnemoviewer.driver.RedisDriver
import ru.intetech.mnemoviewer.model.TagModel
import ru.intetech.mnemoviewer.ui.MnemoViewerUI
import java.awt.Toolkit
import java.util.*

class AlarmsModel(private val userName: String): AutoCloseable {
    data class TableRow(val time: Date, val name: String, val description: String?)

    private val redis = RedisDriver(MnemoViewerUI.redisHost)
    private val redisMonitor: RedisDriver.TagsMonitor

    val tableModel = AlarmsTableModel()
    val alertModel = AlarmsAlertModel(redis, userName)

    init {
        redisMonitor = redis.monitorTags( { onAlarmReceived(it) }, "set", MnemoViewerUI.alarmTags.keys.toSet())
    }

    fun onAlarmReceived(tag: TagModel) {
        if (tag.value.equals(true) || tag.value.equals(1) || tag.value.equals(1.0)) {
            Toolkit.getDefaultToolkit().beep()
            alertModel.onAlarmReceived(tag)
            tableModel.onAlarmReceived(tag)
        }
    }

    override fun close() {
        redisMonitor.close()
        redis.close()
    }
}