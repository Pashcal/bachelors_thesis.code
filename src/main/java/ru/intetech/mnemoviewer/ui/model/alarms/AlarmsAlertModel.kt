package ru.intetech.mnemoviewer.ui.model.alarms

import com.google.gwt.thirdparty.guava.common.annotations.VisibleForTesting
import com.vaadin.ui.UI
import org.joda.time.DateTime
import ru.intetech.mnemoviewer.driver.RedisDriver
import ru.intetech.mnemoviewer.model.TagModel
import ru.intetech.mnemoviewer.ui.MnemoViewerUI

class AlarmsAlertModel(private val redis: RedisDriver = RedisDriver(MnemoViewerUI.redisHost),
                       private val userName: String):
        AlarmsListeningModel() {

    private val confirmKeyPrefix: String
    val unconfirmedAlarms: MutableList<AlarmsModel.TableRow> = mutableListOf()

    init {
        confirmKeyPrefix = "$userName#lastConfirmTime#"
        if (redis.isConnected()) {
            MnemoViewerUI.alarmTags.forEach {
                val confirmDate = redis.get("$confirmKeyPrefix${it.key}")
                val lastValue = redis.getTag(it.key)
                if (lastValue != null) {
                    val lastValueDate = DateTime.parse(lastValue.timestamp)
                    if (confirmDate.isNullOrEmpty() || (!confirmDate.isNullOrEmpty() &&
                            DateTime.parse(confirmDate).isBefore(lastValueDate))) {
                        unconfirmedAlarms.add(0, AlarmsModel.TableRow(lastValueDate.toDate(), lastValue.name,
                                MnemoViewerUI.alarmTags[lastValue.name]))
                    }
                }
            }
        }
    }

    fun confirmAlarm(alarmName: String) {
        unconfirmedAlarms.removeAll { it.name.equals(alarmName) }
        redis.set("$confirmKeyPrefix$alarmName", DateTime.now().toString())
    }

    fun confirmAllAlarms() {
        val date = DateTime.now().toString()
        unconfirmedAlarms.forEach {
            redis.set("$confirmKeyPrefix${it.name}", date)
        }
        unconfirmedAlarms.clear()
    }

    override fun onAlarmReceived(tag: TagModel) {
        val confirmDateTime = redis.get("$confirmKeyPrefix${tag.name}")
        if (confirmDateTime.isNullOrEmpty() || DateTime.parse(confirmDateTime).isBeforeNow) {
            unconfirmedAlarms.add(0, AlarmsModel.TableRow(DateTime.parse(tag.timestamp).toDate(), tag.name,
                    MnemoViewerUI.alarmTags[tag.name]))
            super.onAlarmReceived(tag)
        }
    }
}