package ru.intetech.mnemoviewer.driver

import org.joda.time.DateTime
import redis.clients.jedis.JedisMonitor
import redis.clients.jedis.JedisPool
import redis.clients.jedis.JedisPoolConfig
import ru.intetech.mnemoviewer.model.TagModel
import ru.intetech.mnemoviewer.utils.fromJson
import ru.intetech.mnemoviewer.utils.toJson
import kotlin.concurrent.thread

class RedisDriver(private val host: String, private val port: Int = 6379): AutoCloseable {

    private var jedisPool: JedisPool

    init {
        jedisPool = JedisPool(JedisPoolConfig(), host, port)
    }

    override fun close() {
        jedisPool.close()
        jedisPool.destroy()
    }

    fun isConnected() : Boolean {
        if (jedisPool.isClosed)
                return false
        try {
            jedisPool.resource.use {
                return it.isConnected
            }
        }
        catch (ex: Exception) {
            return false
        }
    }

    fun set(key: String, value: String): Boolean {
        try {
            jedisPool.resource.use {
                it.set(key, value)
            }
            return true
        }
        catch (ex: Exception) {
            println("ERROR! Can't set value in redis: \"$ex\"")
            return false
        }
    }

    fun setTag(tag: TagModel): Boolean {
        return set(tag.name, tag.toJson())
    }

    fun setTag(tagName: String, tagValue: Any): Boolean {
        return setTag(TagModel(name = tagName, value = tagValue, timestamp = DateTime.now().toString()))
    }

    fun get(key: String): String? {
        try {
            jedisPool.resource.use {
                return it.get(key)
            }
        }
        catch (ex: Exception) {
            println("ERROR! Can't get value from redis: \"$ex\"")
            return null
        }
    }

    fun getTag(tagName: String): TagModel? {
        return get(tagName)?.fromJson<TagModel>()
    }

    fun monitorTags(eventHandler: (tag: TagModel) -> Unit, cmdName: String? = null,
                    monitoringTags: Set<String>? = null, monitoringValues: Set<Any>? = null): TagsMonitor {
        val cmdMonitor = TagsMonitor(eventHandler, cmdName, monitoringTags, monitoringValues)
        thread(name = "Redis monitor") {
            try {
                val jedis = jedisPool.resource
                jedis.client.setTimeoutInfinite()
                jedis.monitor(cmdMonitor)
            }
            catch (ex: Exception) {
                println("ERROR! Can't start monitoring redis: \"$ex\"")
            }
        }
        return cmdMonitor
    }

    class TagsMonitor(val eventHandler: (tag: TagModel) -> Unit, var cmdName: String?,
                      val monitoringTags: Set<String>?, val monitoringValues: Set<Any>?): JedisMonitor(), AutoCloseable {

        init {
            cmdName = cmdName?.toLowerCase()
        }

        override fun close() {
            this.client.close()
        }

        override fun onCommand(cmd: String?) {
            val cmdSplit = cmd?.split(' ')
            if (cmdSplit == null || cmdSplit.size < 4)
                return

            val redisCmd = Cmd(cmdSplit[1].trim('"').toLowerCase(), cmdSplit[2].trim('"'), cmdSplit[3].trim('"'))

            if ((cmdName == null || cmdName.equals(redisCmd.name) ) &&
                    (monitoringTags == null || monitoringTags.contains(redisCmd.key))) {
                try {
                    val tagValue = redisCmd.value.replace("\\", "").fromJson<TagModel>()
                    if (monitoringValues == null || monitoringValues.contains(tagValue.value))
                        eventHandler(tagValue)
                } catch (ex: Exception) {
                    println("ERROR! Can't deserialize monitoring tag from redis: \"$ex\"")
                }
            }
        }
    }

    private data class Cmd(val name: String, val key: String, val value: String)
}