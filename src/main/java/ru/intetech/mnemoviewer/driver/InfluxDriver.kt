package ru.intetech.mnemoviewer.driver

import org.influxdb.InfluxDB
import org.influxdb.InfluxDBFactory
import org.influxdb.dto.Query
import org.influxdb.dto.QueryResult
import org.joda.time.DateTime
import ru.intetech.mnemoviewer.model.TagModel

class InfluxDriver(private var host: String, private val user: String = "root",
                   private val password: String = "root",
                   private val database: String = "db_default") {

    private val retentionPolicy = "default"
    private val alarmsSerie = "Alarms"
    private val valueColumn = "value"
    private val timestampColumn = "valueTimestamp"
    private val stringValueColumn = "valueString"
    private val tagNameColumn = "tagName"

    init {
        if (!host.startsWith("http://"))
            host = "http://" + host
        host = host.plus(":8086")
    }

    private fun connect(): InfluxDB? {
        try {
            val influxDB = InfluxDBFactory.connect(host, user, password)
            if (!influxDB.describeDatabases().contains(database))
                influxDB.createDatabase(database)
            return influxDB
        }
        catch (ex: Exception) {
            println("ERROR! Can't connect to influx: \"$ex\"")
            return null
        }
    }

    private fun query(queryString: String): QueryResult {
        val influxDB = connect()
        if (influxDB == null) {
            val qr = QueryResult()
            qr.error = "Error with connection to DB [$database]"
            return qr
        }
        return influxDB.query(Query(queryString, database))
    }

    fun add(tag: TagModel, addToAlarmSerie: Boolean = false): Boolean {
        val influxDB = connect() ?: return false
        try {
            influxDB.write(database, retentionPolicy, InfluxDB.ConsistencyLevel.ALL,
                    tag.toInfluxWriteString(addToAlarmSerie))
            return true
        }
        catch (ex: Exception) {
            println("ERROR! Can't add tag to influx: \"$ex\"")
            return false
        }
    }

    fun add(tagName: String, tagValue: Any, addToAlarmSerie: Boolean = false): Boolean {
        return add(TagModel(name = tagName, value = tagValue, timestamp = DateTime.now().toString()), addToAlarmSerie)
    }

    fun getAll(serieName: String): List<TagModel>? {
        val queryResult = query("select * from $serieName")

        if (queryResult.isNullOrEmptySeries() || queryResult.hasError())
            return null

        val serie = queryResult.results.first().series.first()
        val columnIds = serie.getColumnIds()

        val result = mutableListOf<TagModel>()
        serie.values.forEach {
            result.add(TagModel(if (columnIds.tagNameId == -1) serieName else it[columnIds.tagNameId].toString(),
                    it[columnIds.valueId], it[columnIds.timestampId].toString()))
        }
        return result
    }

    fun getPeriod(serieName: String, startDate: DateTime, endDate: DateTime): List<TagModel>? {
        val queryResult = query("select * from $serieName where " +
                "time >= ${startDate.millis}000000 and time <= ${endDate.millis}000000")

        if (queryResult.isNullOrEmptySeries() || queryResult.hasError())
            return null

        val serie = queryResult.results.first().series.first()
        val columnIds = serie.getColumnIds()

        val result = mutableListOf<TagModel>()
        serie.values.forEach {
            result.add(TagModel(if (columnIds.tagNameId == -1) serieName else it[columnIds.tagNameId].toString(),
                    it[columnIds.valueId], it[columnIds.timestampId].toString()))
        }
        return result
    }

    private fun TagModel.toInfluxWriteString(addToAlarmSerie: Boolean = false): String {
        val serieName = if (addToAlarmSerie) alarmsSerie else this.name
        val tagNameValue = if (addToAlarmSerie) ",$tagNameColumn=${this.name}" else ""

        val values = when (this.value) {
            is Long, is Number, is Double, is Float, is Boolean -> "$tagNameValue $valueColumn=${this.value}"
            else -> "$tagNameValue ,$stringValueColumn=${this.value.toString()} $valueColumn=true"
        }
        return "$serieName,$timestampColumn=${this.timestamp}$values ${DateTime.parse(this.timestamp).millis}000000"
    }

    private fun QueryResult.isNullOrEmptySeries(): Boolean {
        return this.results?.firstOrNull()?.series?.firstOrNull() == null
    }

    data class ColumnIds(val tagNameId: Int, val valueId: Int, val timestampId: Int)
    private fun QueryResult.Series.getColumnIds(): ColumnIds {
        var valueId = this.columns.indexOf(stringValueColumn)
        if (valueId == -1) valueId = this.columns.indexOf(valueColumn)
        return ColumnIds(this.columns.indexOf(tagNameColumn), valueId,
                         this.columns.indexOf(timestampColumn))
    }
}