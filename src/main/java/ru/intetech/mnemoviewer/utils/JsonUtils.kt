package ru.intetech.mnemoviewer.utils

import com.google.gson.Gson
import java.lang.reflect.Type

fun Any.toJson() = Gson().toJson(this)
fun Any.toJson(type: Type) = Gson().toJson(this, type)

inline fun <reified T: Any> String.fromJson() = Gson().fromJson(this, T::class.java)