package ru.intetech.mnemoviewer_datagenerator

import ru.intetech.mnemoviewer.driver.InfluxDriver
import ru.intetech.mnemoviewer.driver.RedisDriver
import ru.intetech.mnemoviewer.utils.fromJson
import java.io.FileReader
import java.nio.file.Paths
import java.util.*
import kotlin.concurrent.timer

class Generator(val host: String) {

    val redis = RedisDriver(host)
    val influx = InfluxDriver(host)
    var timer: Timer? = null
    var isGenerate = false

    val stuffTags: List<String>
    val alarmTags: List<String>

    init {
        val sTags = mutableListOf<String>()
        val aTags = mutableListOf<String>()
        val tagsProp = Properties()
        tagsProp.load(FileReader(Paths.get("tags.properties").toAbsolutePath().toString()))
        tagsProp.forEach {
            p -> run {
                if (p.value.toString().fromJson<PropTag>().isAlarm) {
                    aTags.add(p.key.toString())
                    println("ALARM ${p.key}")
                }
                else {
                    sTags.add(p.key.toString())
                    println("STUFF ${p.key}")
                }
            }
        }
        stuffTags = sTags
        alarmTags = aTags
    }

    fun start(period: Long = 5000) {
        println("Start generating data...")
        isGenerate = true
        timer = timer(name = "MnemoViewer data generator", period = period, action = {
            if (isGenerate) {
                val rand = Random()
//                if (rand.nextInt(10) < 3) {
                    val alarmTagName = alarmTags[rand.nextInt(alarmTags.size)]
                    redis.set(alarmTagName, true)
                    influx.add(alarmTagName, true)
                    influx.add(alarmTagName, true, true)
//                }
//                stuffTags.forEach {
//                    tagName -> run {
//                        val tagValue = 42.plus(rand.nextDouble() * 42)
//                        redis.set(tagName, tagValue)
//                        influx.add(tagName, tagValue)
//                    }
//                }
            }
            else {
                timer?.cancel()
                timer?.purge()
                timer = null
                println("Generating is stopped!")
            }
        })
    }

    fun stop() {
        isGenerate = false
    }

    data class PropTag(val isAlarm: Boolean)
}