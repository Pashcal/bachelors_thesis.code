package ru.intetech.mnemoviewer_datagenerator

fun main(args: Array<String>) {
    if (args.count() < 1) {
        println("Please write db host in arguments.")
        return
    }
    val generator = Generator(args[0])

    var cmd = readLine()
    while (!"exit".equals(cmd)) {
        val cmdSplit = cmd?.split(' ') ?: listOf("")
        when (cmdSplit[0]) {
            "start" -> generator.start(cmdSplit[1].toLong())
            "stop" -> generator.stop()
        }
        cmd = readLine()
    }
    generator.stop()
    return
}