package ru.intetech.mnemoviewer.driver

import org.joda.time.DateTime
import redis.clients.jedis.JedisPool
import redis.clients.jedis.JedisPoolConfig
import ru.intetech.mnemoviewer.model.Tag
import ru.intetech.mnemoviewer.utils.toJson

class RedisDriver(val host: String, val port: Int = 6379): AutoCloseable {

    private var jedisPool: JedisPool

    init {
        jedisPool = JedisPool(JedisPoolConfig(), host, port)
    }

    fun set(tag: Tag) {
        jedisPool.resource.use {
            it.set(tag.name, tag.toJson())
        }
    }

    fun set(tagName: String, tagValue: Any) {
        set(Tag(name = tagName, value = tagValue, timestamp = DateTime.now().toString()))
    }

    override fun close() {
        jedisPool.close()
        jedisPool.destroy()
    }
}