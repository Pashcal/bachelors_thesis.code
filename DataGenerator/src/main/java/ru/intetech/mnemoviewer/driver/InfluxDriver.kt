package ru.intetech.mnemoviewer.driver

import org.influxdb.InfluxDB
import org.influxdb.InfluxDBFactory
import org.joda.time.DateTime
import ru.intetech.mnemoviewer.model.Tag

class InfluxDriver(var host: String, val user: String = "root", val password: String = "root",
                   val database: String = "db_default") {

    private val retentionPolicy = "default"
    private val alarmsSerie = "Alarms"
    private val valueColumn = "value"
    private val timestampColumn = "valueTimestamp"
    private val stringValueColumn = "valueString"
    private val tagNameColumn = "tagName"

    init {
        if (!host.startsWith("http://"))
            host = "http://" + host
        host = host.plus(":8086")
    }

    private fun connect(): InfluxDB? {
        try {
            val influxDB = InfluxDBFactory.connect(host, user, password)
            if (!influxDB.describeDatabases().contains(database))
                influxDB.createDatabase(database)
            return influxDB
        }
        catch (ex: Exception) {
            println("Can't connect to influxDB database [$database]: $ex")
            return null
        }
    }

    fun add(tag: Tag, addToAlarmSerie: Boolean = false): Boolean {
        val influxDB = connect() ?: return false
        try {
            influxDB.write(database, retentionPolicy, InfluxDB.ConsistencyLevel.ALL,
                    tag.toInfluxWriteString(addToAlarmSerie))
            return true
        }
        catch (ex: Exception) {
            println("Can't get tag [${tag.name}] from influxDB [$database]: $ex")
            return false
        }
    }

    fun add(tagName: String, tagValue: Any, addToAlarmSerie: Boolean = false): Boolean {
        return add(Tag(name = tagName, value = tagValue, timestamp = DateTime.now().toString()), addToAlarmSerie)
    }

    private fun Tag.toInfluxWriteString(addToAlarmSerie: Boolean = false): String {
        val serieName = if (addToAlarmSerie) alarmsSerie else this.name
        val tagNameValue = if (addToAlarmSerie) ",$tagNameColumn=${this.name}" else ""

        val values = when (this.value) {
            is Long, is Number, is Double, is Float, is Boolean -> "$tagNameValue $valueColumn=${this.value}"
            else -> "$tagNameValue ,$stringValueColumn=${this.value.toString()} $valueColumn=true"
        }
        return "$serieName,$timestampColumn=${this.timestamp}$values ${DateTime.parse(this.timestamp).millis}000000"
    }
}