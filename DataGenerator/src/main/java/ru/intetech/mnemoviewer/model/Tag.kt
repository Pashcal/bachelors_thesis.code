package ru.intetech.mnemoviewer.model

data class Tag(val name: String, val value: Any, val timestamp: String)